/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Recuperacion1;

/**
 *
 * @author efrai
 */
public class VehiculoCarga extends Vehiculo{
    private float numeroToneladas=0.0f;

    public VehiculoCarga(float numeroToneladas, int serieDeVehiculo, String tipoMotor, String Marca, String Modelo) {
        super(serieDeVehiculo, tipoMotor, Marca, Modelo);
        this.numeroToneladas = numeroToneladas;
    }

    public VehiculoCarga() {
        super();
        this.numeroToneladas = 0.0f;
    }

    public float getNumeroToneladas() {
        return numeroToneladas;
    }

    public void setNumeroToneladas(float numeroToneladas) {
        this.numeroToneladas = numeroToneladas;
    }
    

}
