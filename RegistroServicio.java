/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Recuperacion1;

/**
 *
 * @author efrai
 */
public class RegistroServicio  {
    protected int numServicio;
    protected String fechaServicio;
    protected String tipoServicio;
    protected Vehiculo vehiculo;
    protected String descripcion;
    protected float costoBase;

    public RegistroServicio(int numServicio, String fechaServicio, String tipoServicio, Vehiculo vehiculo, String descripcion, float costoBase) {
        this.numServicio = numServicio;
        this.fechaServicio = fechaServicio;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descripcion = descripcion;
        this.costoBase = costoBase;
    }

    public RegistroServicio() {
        this.numServicio = 0;
        this.fechaServicio ="";
        this.tipoServicio = "";
        this.descripcion = "";
        this.costoBase =0.0f;
    }

    public int getNumServicio() {
        return numServicio;
    }

    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }

    public String getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(String fechaServicio) {
        this.fechaServicio = fechaServicio;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCostoBase() {
        return costoBase;
    }

    public void setCostoBase(float costoBase) {
        this.costoBase = costoBase;
    }
    
    
}
