/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Recuperacion1;

/**
 *
 * @author efrai
 */
public class Vehiculo extends RegistroServicio{
    private int serieDeVehiculo;
    private String tipoMotor;
    private String Marca;
    private String Modelo;

    public Vehiculo(int serieDeVehiculo, String tipoMotor, String Marca, String Modelo) {
        this.serieDeVehiculo = serieDeVehiculo;
        this.tipoMotor = tipoMotor;
        this.Marca = Marca;
        this.Modelo = Modelo;
    }

    public Vehiculo() {
       this.serieDeVehiculo = 0;
       this.tipoMotor = "";
       this.Marca = "";
       this.Modelo = "";
    }

    public int getSerieDeVehiculo() {
        return serieDeVehiculo;
    }

    public void setSerieDeVehiculo(int serieDeVehiculo) {
        this.serieDeVehiculo = serieDeVehiculo;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }
    
    
    
}
